# Open Data
## はじめに
作成したオープンデータを置いています。

# aozora-books.rdf
青空文庫で公開されている情報を元にRDF化したデータです。以下、簡単な構造です。

## Bookリソース
本を表すリソースです。基本的にはDCTermsを利用していてプロパティを定義しています。

タイトル
> dcterms:title

著者
> dcterms:creator

リソースが本であることを示します。dbpediaで定義されたリソースを利用しています。
> rdf:type http://dbpedia.org/ontology/Book

## Autherリソース
著者を表すリソースです。以下のプロパティを持っています。

氏名
> vcard4:hasName

リソースが著者であることを示します。dbpediaで定義されたリソースを利用しています。
> rdf:type http://dbpedia.org/ontology/author

# aozora-books-with-dbpedia.rdf
aozora-books.rdfの内容にDBPediaの情報を追加したものです。

983の著者リソースのうち、DBPediaから一致するリソースを抽出出来た260リソースに対して、owl:sameAsで関係性を結んでいます。
